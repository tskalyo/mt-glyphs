#!/bin/bash

# Generate texture overlays.

MODNAME="glyphs"

if [[ -z "${FONT}" ]]; then
  FONT="DejaVu-Sans-Book"
fi

if [[ -z "${FONT_SIZE}" ]]; then
  FONTSIZE=16
fi

if [[ -z "${CODES}" ]]; then
  CODES="$(
    seq 0x0030 0x0039 # Numbers
    seq 0x0041 0x005A # Uppercase letters
    seq 0x0061 0x007A # Lowercase letters
    seq 0x2190 0x2193 # Arrows
  )"
fi

if ! command -v convert > /dev/null 2>&1; then
  >&2 printf "error: this script requires the convert binary from ImageMagick\n"
  exit 1
fi

for code in ${CODES}; do
  hex="$(printf "%04x" "${code}")"
  printf "%b" "\u${hex}" \
    | convert \
      -size "${FONTSIZE}"x"${FONTSIZE}" \
      -pointsize "${FONTSIZE}" \
      -gravity center \
      -background none \
      -fill white \
      -font "${FONT}" \
      label:@- \
      textures/"${MODNAME}"_u"${hex}"_overlay.png
done
