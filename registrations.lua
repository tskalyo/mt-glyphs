local default_nodes = {
  {"cobble"},
  {"mossycobble"},
  {"desert_cobble"},
  {"stone"},
  {"desert_stone"},
  {"sandstone"},
  {"desert_sandstone"},
  {"silver_sandstone"},
  {"brick"},
  {"obsidian"},
  {"tree"},
  {"wood"},
  {"jungletree"},
  {"junglewood"},
  {"pine_tree"},
  {"pine_wood"},
  {"acacia_tree"},
  {"acacia_wood"},
  {"aspen_tree"},
  {"aspen_wood"},
  {"coalblock",    "coal_block"},
  {"steelblock",   "steel_block"},
  {"tinblock",     "tin_block"},
  {"copperblock",  "copper_block"},
  {"bronzeblock",  "bronze_block"},
  {"goldblock",    "gold_block"},
  {"mese",         "mese_block"},
  {"diamondblock", "diamond_block"},
}

for _, row in ipairs(default_nodes) do
  local nodename = "default:" .. row[1]
  local texture = "default_" .. (row[2] or row[1]) .. ".png"

  local ndef = minetest.registered_nodes[nodename]
  glyphs.register_material(nodename, ndef.description, texture)
end
