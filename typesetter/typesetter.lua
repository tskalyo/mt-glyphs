local typesetter = {}

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local config_dialog = dofile(modpath .. "/typesetter/config_dialog.lua")

-- Use custom utf8 functions if the builtin library is not available.
if not utf8 then
  local utf8 = dofile(modpath .. "/utf8.lua")
end

--[[
Given a string, return the first glyph, the consumed portion of the string, and
the remaining portion of the string.

A glyph can be a:
- single character (e.g. "A"),
- 4-digit unicode code point (e.g. "\u0041")
- 8-digit unicode code point (e.g. "\U00000041")
--]]
function typesetter.consume(str)
  local glyph
  local consumed = str:match("\\u%x%x%x%x") or str:match("\\U%x%x%x%x%x%x%x%x")

  if consumed then
    local code = consumed:sub(3, #consumed)
    glyph = utf8.char(tonumber(code, 16))
  end

  if not glyph or glyph == "" then
    consumed = str:sub(1, 1)
    glyph = consumed
  end

  return glyph, consumed, str:sub(#consumed + 1, #str)
end

-- Show config dialog on left click.
function typesetter.on_use(itemstack, user, pointed_thing)
  config_dialog.show_to(user)
  return nil
end

-- Place next glyph node on right click.
function typesetter.on_place(itemstack, placer, pointed_thing)

  -- Must be pointed at a node.
  local player_name = placer:get_player_name();
  if pointed_thing.type ~= "node" then
    minetest.chat_send_player(player_name, "Error: No node.")
    return nil
  end
  local pos = minetest.get_pointed_thing_position(pointed_thing, true)
  local node = minetest.get_node_or_nil(pos)
  if not node then
    minetest.chat_send_player(player_name, "Error: No node.")
    return nil
  end

  -- Must have material selected.
  local meta = itemstack:get_meta()
  local material_node_name = meta:get_string("material")
  local material = glyphs.materials[material_node_name]
  if not material or material == "" then
    minetest.chat_send_player(player_name, "Left click to select material.")
    return nil
  end

  -- Must have text configured.
  local glyph_str, consumed, remaining = typesetter.consume(meta:get_string("text"))
  if not glyph_str or glyph_str == "" then
    minetest.chat_send_player(player_name, "Left click to input text.")
    return nil
  end

  -- Typed text must be valid.
  local glyph = glyphs.glyphs[glyph_str]
  if not glyph then
    minetest.chat_send_player(
      player_name,
      "Warning: skipping unsupported character '" .. consumed .. "'."
    )
    meta:set_string("text", remaining)
    return itemstack
  end

  local glyph_node_name = material_node_name .. "_u" .. glyph.code
  local has_glyph_node = placer:get_inventory():contains_item("main", glyph_node_name)
  local has_material_node = placer:get_inventory():contains_item("main", material_node_name)

  -- Must have the required material node if not creative.
  if not minetest.settings:get_bool("creative_mode") and
    not minetest.check_player_privs(player_name, {creative=true}) and
    not has_glyph_node and
    not has_material_node then

    minetest.chat_send_player(
      player_name,
      "You have no further '" .. material_node_name .. "'."
    )
    return nil
  end

  -- Use existing glyph nodes before creating new ones.
  if has_glyph_node then
    material_node_name = glyph_node_name
  end

  -- Consume material node and place the glyph node.
  placer:get_inventory():remove_item("main", material_node_name .. " 1")
  minetest.add_node(pos, {
    name = glyph_node_name,
    param2 = minetest.dir_to_wallmounted(placer:get_look_dir()),
  })
  meta:set_string("text", remaining)

  return itemstack
end

minetest.register_tool(modname .. ":typesetter", {
  description = "Typesetter, left-click to configure, right-click to use",
  groups = {},
  inventory_image = modname .. "_typesetter.png",
  wield_image = "",
  wield_scale = {x=1, y=1, z=1},
  stack_max = 1,
  liquids_pointable = false,
  node_placement_prediction = nil,
  on_use = typesetter.on_use,
  on_place = typesetter.on_place,
})

minetest.register_craft({
  output = modname .. ":typesetter",
  recipe = {
    { "default:steel_ingot", "default:steel_ingot", "default:steel_ingot" },
    { "default:stick",       "default:paper",       "default:stick"       },
    { "default:steel_ingot", "default:steel_ingot", "default:steel_ingot" },
  }
})
