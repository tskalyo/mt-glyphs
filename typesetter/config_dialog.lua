local config_dialog = {
  contexts = {}
}

local modname = minetest.get_current_modname()

-- Get cached context or create one from wielded typesetter tool.
function config_dialog.get_context(player)
  local player_name = player:get_player_name()

  if not config_dialog.contexts[player_name] then
    local context = {}

    local itemstack = player:get_wielded_item()
    if itemstack:get_name() == modname .. ":typesetter" then
      local meta = itemstack:get_meta()
      context = {
        material = meta:get_string("material"),
        text = meta:get_string("text"),
      }
    end

    if context.material == "" then
      context.material = "default:cobble"
    end
    config_dialog.contexts[player_name] = context
  end

  return config_dialog.contexts[player_name]
end

-- Get a formspec dropdown list string that represents the available materials.
function config_dialog.get_material_dd(selected)
  local material_names = {}
  local selected_idx = 1
  for name, _ in pairs(glyphs.materials) do
    local i = #material_names + 1
    material_names[i] = name
    if name == selected then
      selected_idx = i
    end
  end

  return table.concat(material_names, ",") .. ";" .. selected_idx
end

-- Get config formspec for in the given context.
function config_dialog.get_formspec(context)
  local material_dd = config_dialog.get_material_dd(context.material)
  local material_image = glyphs.materials[context.material].tile
  local text = context.text or ""
  local message = context.message or ""

  local formspec = {
    "formspec_version[4]",
    "size[6.75,6]",
    "image[0.375,0.375;1,1;", material_image, "]",
    "dropdown[1.75,0.575;4.625,0.8;material;", material_dd, "]",
    "field[0.375,2.25;6,0.8;text;Text;", minetest.formspec_escape(text), "]",
    "label[0.375,3.5;", minetest.formspec_escape(message), "]",
    "button_exit[2.125,4.5;3,0.8;save;Save]",
  }

  return table.concat(formspec)
end

-- Show config.
function config_dialog.show_to(player)
  local player_name = player:get_player_name()
  local context = config_dialog.get_context(player)
  local config_fs = config_dialog.get_formspec(context)

  minetest.show_formspec(player_name, "typesetter:config", config_fs)
end

-- Clear a player's context when they leave.
minetest.register_on_leaveplayer(function(player)
  config_dialog.contexts[player:get_player_name()] = nil
end)

-- Update context and wielded typesetter tool.
minetest.register_on_player_receive_fields(function(player, formname, fields)
  if formname ~= "typesetter:config" then
    return
  end

  local itemstack = player:get_wielded_item()
  if itemstack:get_name() ~= modname .. ":typesetter" then
    config_dialog.contexts[player:get_player_name()] = nil
    return
  end

  local context = config_dialog.get_context(player)
  context.material = fields.material
  context.text = fields.text

  if fields.save then
    local meta = itemstack:get_meta()
    meta:set_string("text", context.text)
    meta:set_string("material", context.material)

    player:set_wielded_item(itemstack)
  end

  if fields.quit then
    config_dialog.contexts[player:get_player_name()] = nil
    return
  end

  config_dialog.show_to(player)
end)

return config_dialog
