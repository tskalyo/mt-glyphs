-- Minetest currently doesn't include the lua utf8 builtin library (see
-- https://github.com/minetest/minetest/issues/10939). This file reimplements
-- the necessary functions in lua.
utf8 = {}

-- Possible first-byte bit masks for each code point range.
local bit_masks = {
  {0x80,     0x00}, -- 0xxxxxxx
  {0x7FF,    0xC0}, -- 110xxxxx
  {0xFFFF,   0xE0}, -- 1110xxxx
  {0x1FFFFF, 0xF0}, -- 11110xxx
}

--[[
Converting a unicode code point to a utf-8 string requires setting the
appropriate bit masks. They indiciate how many bytes are being used.

Code point <-> UTF-8 conversion

| Code Points    | Avail Bits | Byte 1   | Byte 2   | Byte 3   | Byte 4   |
| ---            | ---        | ---      | ---      | ---      | ---      |
| 0000  - 007F   | 7          | 0xxxxxxx |          |          |          |
| 0080  - 07FF   | 11         | 110xxxxx | 10xxxxxx |          |          |
| 0800  - FFFF   | 16         | 1110xxxx | 10xxxxxx | 10xxxxxx |          |
| 10000 - 10FFFF | 21         | 11110xxx | 10xxxxxx | 10xxxxxx | 10xxxxxx |

The first byte indicates the unicode encoding length. The first bit is set if
using multiple bytes. Subsequent bits indicate the number of bytes used.
-- ]]
local function code_to_string(code)
  local byte_array = {}
  for byte_count, row in ipairs(bit_masks) do
    local upper_bound = row[1]
    local mask = row[2]

    if code <= upper_bound then
      for b=byte_count, 2, -1 do
        local mod = code % 0x40
        code = (code - mod) / 0x40

        -- Each byte following the first byte in a multi-byte encoding will
        -- always start with 0x80 (10xxxxxx).
        byte_array[b] = string.char(0x80 + mod)
      end

      byte_array[1] = string.char(mask + code)
      return table.concat(byte_array)
    end
  end
end

--[[
Receives zero or more integers, converts each one to its corresponding UTF-8
byte sequence and returns a string with the concatenation of all these
sequences.
-- ]]
function utf8.char(...)
  local codes = {...}
  local result = {}

  for i, code in ipairs(codes) do
    result[i] = code_to_string(code)
  end
  return table.concat(result)
end

return utf8
