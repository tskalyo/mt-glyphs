local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath .. "/api.lua")
dofile(modpath .. "/registrations.lua")
dofile(modpath .. "/typesetter/typesetter.lua")
