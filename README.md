# Glyphs Minetest Mod

This mod adds signlike nodes for various glyphs (letters, numbers, and symbols).

![Screenshot of glyph nodes](screenshot.png)

## Typesetter Tool

A typesetter tool is used to place glyph nodes into the world. Left click to choose a material and compose text. Right click to place the next glyph.

Each glyph node costs 1 item of the source material to create. Placing a glyph in your crafting grid allows you to recycle it back into the source material.

A typesetter tool can be crafted using 6 steel ingots, 2 sticks, and a piece of paper.

![Screenshot of typesetter tool recipe](typesetter/screenshot.png)

## Generating Textures

This mod comes with textures for a small subset of unicode glyphs (latin letters, numbers, and a handful of symbols). Additional characters can be generated using `scripts/generate_textures.sh`.

Pre-requisites:
- Bash 4.2 or newer
- The `convert` binary from ImageMagick.
- A compatible font. Use `identify -list font` to list the fonts recognized by ImageMagick.

### Running The Script

Running the `generate_textures.sh` script will (as the name implies) generate textures for this mod. Existing textures will be overwritten.

```bash
./scripts/generate_textures.sh
```

By default the script generates textures for only a small list of unicode code points. Pass in a `CODES` environment variable to override this list.

```bash
export CODES="$(
  seq 0x0021 0x007E # Basic Latin
  seq 0x00A0 0x00FF # Latin-1 Supplement
)"
./scripts/generate_textures.sh
```

By default the script uses the `DejaVu-Sans-Book` font and generates 16x16 textures. Pass in `FONT` and `FONT_SIZE` to override these settings.

```bash
export FONT="DejaVu-Serif-Book"
export FONT_SIZE=32
./scripts/generate_textures.sh
```

## Registering New Materials

Like the fantastic [letters](https://github.com/minetest-mods/letters) mod, this mod supports registering custom materials. Simply include this mod as a dependency and then call:

```lua
glyphs.register_material(name, description, tile, def)
```

Examples can be found in `registrations.lua`, which registers a handful of default textures as new materials.

## Inspiration And Similar Projects

- [letters](https://github.com/minetest-mods/letters)
- [ehlphabet](https://git.bananach.space/ehlphabet.git/about/)
- [abjphabet](https://forum.minetest.net/viewtopic.php?f=11&t=11744)
- [alphabet](https://forum.minetest.net/viewtopic.php?id=2312)
