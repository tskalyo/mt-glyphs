glyphs = {
  materials = {},
}

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Use custom utf8 functions if the builtin library is not available.
if not utf8 then
  local utf8 = dofile(modpath .. "/utf8.lua")
end

-- Populate the glyphs.glyphs table.
local function load_glyphs()
  glyphs.glyphs = {}

  for _, tile in ipairs(minetest.get_dir_list(modpath .. "/textures", false)) do
    local code = tile:match("u(%x%x%x%x%x?%x?)_overlay")
    if code then
      local glyph_str = utf8.char(tonumber(code, 16))
      glyphs.glyphs[glyph_str] = {
        tile = tile,
        code = code,
      }
    end
  end
end

-- Given a base material, register new glyph nodes.
function glyphs.register_material(name, description, tile, def)
  if not minetest.registered_nodes[name] then
    minetest.log(
      "Warning: cannot register '" .. name .. "' as a " .. modname ..
      " material: please register with minetest.register_node() first."
    )
    return
  end

  glyphs.materials[name] = {
    description = description,
    tile = tile,
  }

  -- Copy def and set missing fields.
  local base_def = {}
  if def then
    base_def = table.copy(def)
  end
  if not base_def.paramtype2 then
    base_def.paramtype2 = "wallmounted"
  end
  if not base_def.groups then
    base_def.groups = {
      not_in_creative_inventory = 1,
      not_in_craft_guide = 1,
      oddly_breakable_by_hand = 1,
      attached_node = 1
    }
  end

  -- Override base_def values.
  base_def.sunlight_propagates = true
  base_def.drawtype = "signlike"
  base_def.paramtype = "light"
  base_def.walkable = false
  base_def.is_ground_content = false
  base_def.selection_box = { type = "wallmounted" }
  base_def.legacy_wallmounted = false

  -- Register a node for each glyph.
  for glyph_str, glyph in pairs(glyphs.glyphs) do
    local glyph_node_def = table.copy(base_def)
    local glyph_node_name = name .. "_u" .. glyph.code
    local glyph_node_tile = tile .. "^[mask:" .. glyph.tile

    glyph_node_def.description = description .. " " .. glyph_str
    glyph_node_def.inventory_image = glyph_node_tile
    glyph_node_def.wield_image = glyph_node_tile
    glyph_node_def.tiles = { glyph_node_tile }

    minetest.register_node(":" .. glyph_node_name, glyph_node_def)

    -- Allow glyph nodes to be turned back into their original material.
    minetest.register_craft({
      type = "shapeless",
      output = name,
      recipe = { glyph_node_name },
    })
  end
end

load_glyphs()
